from django.shortcuts import render
import requests
from bs4 import BeautifulSoup


def scrapewiki(url):
    response = requests.get(url=url)
    soup = BeautifulSoup(response.content, 'html.parser')
    paragraphlist = []
    for paragraph in soup.find_all("p"):
        paragraphlist.append(paragraph.text)
    if len(paragraphlist) > 1:
        paragraph1 = paragraphlist[1]
    else:
        paragraph1 = paragraphlist[0]
    sentences = paragraph1.split(".")
    bestsentence = sentences[0]
    bestsentence.replace("\n", " ")
    if any(letter.isalpha() for letter in bestsentence):
        bestsentence = f"{bestsentence}."
        return bestsentence
    else:
        bestsentence = "Who was Queen Elizabeth?"
        return bestsentence


# scrapewiki("https://en.wikipedia.org/wiki/Special:Random")


def home(request):
    sentence = None
    if request.method == "GET":
        sentence = scrapewiki("https://en.wikipedia.org/wiki/Special:Random")
        return render(request, "core/home.html", {"bestsentence": sentence})
    else:
        return render(request, "core/home.html")
